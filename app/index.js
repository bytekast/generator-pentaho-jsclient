'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');


var PentahoJsclientGenerator = module.exports = function PentahoJsclientGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);
  this.argument('appname', { type: String, required: false });
  this.appname = this.appname || path.basename(process.cwd());
  this.appname = this._.camelize(this._.slugify(this._.humanize(this.appname)));

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(PentahoJsclientGenerator, yeoman.generators.Base);

PentahoJsclientGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);

  var prompts = [{
    type: 'confirm',
    name: 'someOption',
    message: 'Would you like to enable this option?',
    default: true
  }];

  this.prompt(prompts, function (props) {
    this.someOption = props.someOption;

    cb();
  }.bind(this));
};

PentahoJsclientGenerator.prototype.app = function app() {

  this.copy('../seed/Gruntfile.js','Gruntfile.js');
  this.copy('../seed/karma.conf.js','karma.conf.js');
  this.copy('../seed/karma-e2e.conf.js','karma-e2e.conf.js');
  //this.copy('../seed/package.json','package.json');

  this.mkdir('app');
  this.copy('../seed/app/.buildignore', 'app/.buildignore');
  this.copy('../seed/app/.htaccess', 'app/.htaccess');
  this.copy('../seed/app/404.html', 'app/404.html');
  this.copy('../seed/app/favicon.ico', 'app/favicon.ico');
  this.copy('../seed/app/index.html', 'app/index.html');
  this.copy('../seed/app/robots.txt', 'app/robots.txt');

  //this.mkdir('app/templates');

  this.mkdir('app/scripts');
  this.copy('../seed/app/scripts/app.js', 'app/scripts/app.js');
  this.copy('../seed/app/scripts/bootstrap.js', 'app/scripts/bootstrap.js');

  // copy controllers
  this.mkdir('app/scripts/controllers');
  this.copy('../seed/app/scripts/controllers/main.js', 'app/scripts/controllers/main.js');
  this.copy('../seed/app/scripts/controllers/controllers.js', 'app/scripts/controllers/controllers.js');

  // copy directives
  this.mkdir('app/scripts/directives');
  this.copy('../seed/app/scripts/directives/directives.js', 'app/scripts/directives/directives.js');

  // copy filters
  this.mkdir('app/scripts/filters');
  this.copy('../seed/app/scripts/filters/filters.js', 'app/scripts/filters/filters.js');

  // copy services
  this.mkdir('app/scripts/services');
  this.copy('../seed/app/scripts/services/services.js', 'app/scripts/services/services.js');

  this.mkdir('app/styles');
  this.copy('../seed/app/styles/bootstrap.css', 'app/styles/bootstrap.css');
  this.copy('../seed/app/styles/main.css', 'app/styles/main.css');

  this.mkdir('app/views');
  this.copy('../seed/app/views/main.html', 'app/views/main.html');


  /***** TEST ****/

  this.mkdir('test');
  this.copy('../seed/test/.jshintrc', 'test/.jshintrc');
  this.copy('../seed/test/runner.html', 'test/runner.html');

  this.mkdir('test/spec');
  this.copy('../seed/test/spec/main.js', 'test/spec/main.js');

  this.mkdir('test/spec/controllers');
  this.copy('../seed/test/spec/controllers/mainSpec.js', 'test/spec/controllers/mainSpec.js');

  this.template('_package.json', 'package.json');
  this.template('_bower.json', 'bower.json');
};

PentahoJsclientGenerator.prototype.projectfiles = function projectfiles() {
  //this.copy('editorconfig', '.editorconfig');
  //this.copy('jshintrc', '.jshintrc');
  this.copy('bowerrc', '.bowerrc');
};
