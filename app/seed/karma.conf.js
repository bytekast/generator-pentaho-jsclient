// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine', 'requirejs', 'ng-scenario'],

    // list of files / patterns to load in the browser
    files: [

      {pattern: 'app/bower_components/angular/angular.js', included: false},
      {pattern: 'app/bower_components/angular-mocks/angular-mocks.js', included: false},

      {pattern: 'app/bower_components/angular-cookies/angular-cookies.js', included: false},
      {pattern: 'app/bower_components/angular-resource/angular-resource.js', included: false},
      {pattern: 'app/bower_components/angular-sanitize/angular-sanitize.js', included: false},
      {pattern: 'app/bower_components/angular-scenario/angular-scenario.js', included: false},
      {pattern: 'app/bower_components/json3/build.js', included: false},
      {pattern: 'app/bower_components/requirejs-domready/domReady.js', included: false},
      {pattern: 'app/bower_components/es5-shim/es5-shim.js', included: false},
      {pattern: 'app/bower_components/json3/build.js', included: false},
      //{pattern: 'app/bower_components/requirejs/require.js', included: false},
      {pattern: 'app/bower_components/jquery/jquery.js', included: false},


       // all the sources, tests  // !! all src and test modules (included: false)
      {pattern: 'app/scripts/controllers/*.js', included: false},
      {pattern: 'test/spec/controllers/*.js', included: false},

      //'app/scripts/*.js',
      //'app/scripts/**/*.js',
      //'test/mock/**/*.js',
      //'test/spec/controllers/*.js',

      'test/spec/main.js'
    ],

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
