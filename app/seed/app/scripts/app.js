// Main App
define(['angular',
  'controllers/controllers',
  'controllers/main',
  'services/services',
  'filters/filters',
  'directives/directives'], function (angular) {

  // Depend only on the main controllers, services, filters and directives modules
  var app = angular.module('pentahoAngularSeedApp', ['controllers', 'services', 'filters', 'directives']);

  app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);

  return app;
});

