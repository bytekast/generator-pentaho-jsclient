// Main Controller
define(['controllers/controllers'],
  function(controllers) {
  	'use strict';

    // Add main controller to the controllers module definition
    controllers.controller('MainCtrl', ['$scope', function ($scope) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma',
        'RequireJS'
	    ];
	  }]);
});

